<?php

// Meant for copying into /devel/php

use Drupal\Component\Serialization\Yaml;
use Drupal\user\Entity\Role;

$roles = Role::loadMultiple();
foreach ($roles as $role_id => $role) {
  $permissions = $role->getPermissions();

  $override = [
    'path' => ['permissions'],
    'plugin' => 'add',
    'actions' => [],
  ];

  foreach ($permissions as $permission) {
    $override['actions'][$permission] = [
      'value' => $permission,
    ];
  }

  if (!empty($override['actions'])) {
    $override = Yaml::encode($override);
    dsm('Save as: config/actions/user.role.' . $role_id . '.yml');
    dsm($override);
  }
}

