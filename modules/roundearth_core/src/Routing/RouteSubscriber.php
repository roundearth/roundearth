<?php

namespace Drupal\roundearth_core\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Modify core routes to support redirect.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Disable the route normalizer from the Redirect module on Angular pages
    // at /civicrm/a for maximum browser compatibility.
    if ($route = $collection->get('civicrm.civicrm_a')) {
      $route->setDefault('_disable_route_normalizer', TRUE);
    }
  }

}
