<?php

namespace Drupal\roundearth_migration\Plugin\migrate\source\d6;

use Drupal\user\Plugin\migrate\source\d6\User as UserD6;

/**
 * Custom D6 user source.
 */
class User extends UserD6 {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    if (!empty($this->configuration['uids'])) {
      $uids = $this->configuration['uids'];
      $query = $query->condition('u.uid', $uids, 'IN');
    }
    return $query;
  }

}