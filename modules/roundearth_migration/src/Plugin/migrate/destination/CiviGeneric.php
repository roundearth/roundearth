<?php

namespace Drupal\roundearth_migration\Plugin\migrate\destination;

use Drupal\roundearth_migration\CiviCRM\CiviCrmAwareTrait;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Generic CiviCRM object destination.
 *
 * @MigrateDestination(
 *   id = "roundearth_migration_civicrm_generic"
 * )
 */
class CiviGeneric extends DestinationBase {

  use CiviCrmAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return ['id' => ['type' => 'integer']];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'id' => $this->t('ID.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $data = [];

    $id = $row->getDestinationProperty('id');
    if (!$id) {
      $id = reset($old_destination_id_values);
    }

    if ($id && $this->load($this->getType(), $id)) {
      // Updating.
      $data['id'] = $id;
      $this->setRollbackAction($row->getIdMap());
    }

    foreach ($row->getDestination() as $property => $value) {
      $data[$property] = $value;
    }

    $this->getCivi()->initialize();
    $result = civicrm_api3($this->getType(), 'create', $data);
    return [$result['id']];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsRollback() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $this->getCivi()->initialize();
    civicrm_api3($this->getType(), 'delete', array(
      'id' => reset($destination_identifier),
    ));
  }

  /**
   * @param $type
   * @param $id
   */
  protected function load($type, $id) {
    try {
      $this->getCivi()->initialize();
      $result = civicrm_api3($type, 'getsingle', ['id' => $id]);
    } catch (\Exception $e) {
      $result = NULL;
    }

    return $result;
  }

  /**
   * Gets the CiviCRM object type.
   *
   * @return string
   *   The CiviCRM object type.
   */
  protected function getType() {
    return $this->configuration['type'];
  }

}
