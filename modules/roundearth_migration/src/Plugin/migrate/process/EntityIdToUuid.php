<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_entity_id_to_uuid"
 * )
 */
class EntityIdToUuid extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $entity_type = $this->configuration['entity_type'];
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($value);
    if ($entity) {
      return $entity->uuid();
    }
    return NULL;
  }

}