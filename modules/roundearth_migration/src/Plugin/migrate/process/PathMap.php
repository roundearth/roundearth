<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class PathMap.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_path_map"
 * )
 */
class PathMap extends ProcessPluginBase {

  /**
   * @var array
   * @TODO: add users, vocabs, etc.
   */
  protected $defaultConfiguration = [
    'node_migrations' => [],
    'taxonomy_term_migrations' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $parts = explode('/', trim($value, '/'));

    if (count($parts) == 2 && $parts[0] == 'node') {
      return $this->transformNode($parts);
    }

    if (count($parts) == 3 && $parts[0] == 'taxonomy' && $parts[1] == 'term') {
      return $this->transformTaxonomyTerm($parts);
    }

    return $value;
  }

  /**
   * @param array $parts
   * @return string
   */
  public function transformNode($parts) {
    $nid = $this->getMigrateMapUtil()->lookupSourceId($parts[1], $this->getConfig()['node_migrations']);

    if ($nid) {
      $parts[1] = $nid;
    }

    return implode('/', $parts);
  }

  /**
   * @param array $parts
   * @return string
   */
  public function transformTaxonomyTerm($parts) {
    $tid = $this->getMigrateMapUtil()->lookupSourceId($parts[2], $this->getConfig()['taxonomy_term_migrations']);

    if ($tid) {
      $parts[2] = $tid;
    }

    return implode('/', $parts);
  }

  /**
   * @return array
   */
  protected function getConfig() {
    $config = [];
    foreach ($this->defaultConfiguration as $key => $value) {
      $item = $this->getSettings()->get($key);
      $config[$key] =  $item !== NULL ? $item : $this->defaultConfiguration[$key];
    }
    return $config;
  }

  /**
   * @return \Drupal\Core\Config\ImmutableConfig
   */
  protected function getSettings() {
    return \Drupal::config('roundearth_migration.settings');
  }

  /**
   * @return \Drupal\roundearth_migration\MigrateMapUtil
   */
  protected function getMigrateMapUtil() {
    return \Drupal::service('roundearth_migration.migrate_map_util');
  }

}
