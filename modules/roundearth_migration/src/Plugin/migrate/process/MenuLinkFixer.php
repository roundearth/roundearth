<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\roundearth_migration\MigrateMapUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MenuLinkFixer.
 *
 * In it's current state this is essentially a key-value lookup just like the
 * "static_map" plugin, but works with menu paths as keys.
 *
 * In the future, we may wish to add more analysis logic and cases.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_menu_link_fixer"
 * )
 */
class MenuLinkFixer extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Direct mappings for paths.
   *
   * @var array
   */
  protected $pathMap = [];

  /**
   * ID map utility service.
   *
   * @var \Drupal\roundearth_migration\MigrateMapUtil
   */
  protected $mapUtil;

  /**
   * Constructs a MenuLinkFixer.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\roundearth_migration\MigrateMapUtil $map_util
   * @param array $path_map
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrateMapUtil $map_util, $path_map) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mapUtil = $map_util;
    if ($path_map) {
      $this->pathMap = $path_map;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('roundearth_migration.migrate_map_util'),
      $container->get('config.factory')->get('roundearth_migration.settings')->get('path_map')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      list($title, $path) = $value;
    }
    else {
      throw new \Exception(sprintf('Invalid value provided for "%s" property.', $destination_property));
    }

    if (!$title || !$path) {
      $id = implode('-', $row->getSourceIdValues());
      throw new MigrateSkipRowException(sprintf('Invalid menu item title/path, source id %s.', $id));
    }

    if ($mapped = $this->doPathMap($path)) {
      return $mapped;
    }

    $path = $this->fixTaxonomyTerms($path);

    // If not valid, mark the row as needing updated.
    if (!$this->validatePath($path)) {
      $path = '<front>';
      $title = trim('* ' . $title);
    }

    return [
      'title' => $title,
      'path' => $path,
    ];
  }

  /**
   * Map the path.
   *
   * @param string $value
   *   The original path.
   *
   * @return string|null
   *   The mapped path, or NULL if not available.
   */
  protected function doPathMap($value) {
    if (isset($this->pathMap[$value])) {
      return $this->pathMap[$value];
    }

    return NULL;
  }

  /**
   * Determine if the path is valid.
   *
   * @param string $path
   *   The path to be tested for validity.
   *
   * @return bool
   *   Indicates if the path is valid.
   *
   * @see \Drupal\menu_link_content\Plugin\migrate\process\LinkUri::transform
   */
  protected function validatePath($path) {
    $path = ltrim($path, '/');

    if (parse_url($path, PHP_URL_SCHEME) !== NULL) {
      return TRUE;
    }

    if ($path == '<front>') {
      return TRUE;
    }

    $path = 'internal:/' . $path;
    $url = Url::fromUri($path);
    return $url->isRouted();
  }

  /**
   * Fix paths for taxonomy terms.
   *
   * @param string $path
   *   The original path.
   *
   * @return string
   *   The updated path.
   */
  protected function fixTaxonomyTerms($path) {
    $originalPath = $path;
    $path = explode('/', ltrim($path, '/'));

    if (count($path) != 3 || $path[0] != 'taxonomy' || $path[1] != 'term') {
      return $originalPath;
    }

    if ($id = $this->mapUtil->lookupDestId($path[2], $this->configuration['taxonomy_migrations'])) {
      $path[3] = $id;
    }

    return implode('/', $path);
  }

}
