<?php

use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Implements hook_theme().
 */
function roundearth_theme_theme() {
  $theme = [];

  // Add theme for civicrm entities.
  $config = \Drupal::config('civicrm_entity.settings');
  $enabled_entity_types = $config->get('enabled_entity_types') ?: [];
  foreach ($enabled_entity_types as $entity_type_id => $entity_type_info) {
    $theme[$entity_type_id] = [
      'render element' => 'elements',
    ];
  }

  return $theme;
}

/**
 * Implements hook_preprocess().
 */
function roundearth_theme_preprocess(&$variables, $hook) {
  $config = \Drupal::config('civicrm_entity.settings');
  $enabled_entity_types = $config->get('enabled_entity_types') ?: [];

  // Add drupal fields as content to template for civicrm entities.
  if (isset($enabled_entity_types[$hook])) {
    $variables += ['content' => []];
    foreach (Element::children($variables['elements']) as $key) {
      $variables['content'][$key] = $variables['elements'][$key];
    }
  }
}

/**
 * Implements hook_toolbar_alter().
 */
function roundearth_theme_toolbar_alter(&$items) {
  if (!isset($items['civicrm'])) {
    return;
  }

  // Because we're making changes based on the path, we need
  // to change the cache contexts.
  $items['civicrm']['#cache']['contexts'][] = 'url.path';

  // Make sure the id is set.
  $items['civicrm']['tab']['#options']['attributes']['id'] = 'toolbar-item-civicrm';

  $path = \Drupal::routeMatch()->getRouteObject()->getPath();
  $patterns = "/civicrm\n/civicrm/*";

  if (\Drupal::service('path.matcher')->matchPath($path, $patterns)) {
    // Add a dummy tray so that the tab triggers a tray open.
    unset($items['civicrm']['tab']['#options']);
    $items['civicrm']['tray'] = [
      '#type' => 'link',
      '#title' => t('CiviCRM'),
      '#url' => Url::fromRoute('civicrm.civicrm'),
    ];
  }
}

/**
 * Implements hook_preprocess_toolbar().
 */
function roundearth_theme_preprocess_toolbar(&$variables) {
  // Attach our custom library.
  $variables['#attached']['library'][] = 'roundearth_theme/toolbar';
}
